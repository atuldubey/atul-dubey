-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2013 at 05:09 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `a_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `friend_id` int(11) NOT NULL DEFAULT '0',
  `state` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`request_id`, `user_id`, `friend_id`, `state`) VALUES
(36, 1, 3, 2),
(37, 8, 1, 2),
(38, 9, 2, 2),
(39, 1, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `userid`, `friend_id`, `message`, `time`) VALUES
(45, 1, 3, 'Wow... I didn''t know that the previous message was sent...', 1370687424),
(46, 3, 1, 'Oh.. I got the mail...', 1370687799),
(47, 1, 3, 'Finally the messages are being displayed properly...', 1370688563),
(48, 1, 3, ':)', 1370688568),
(49, 3, 1, 'I got your message perfectly...', 1370688658),
(50, 2, 9, 'Hie... How are you??', 1370711596),
(51, 1, 3, 'Hie...', 1370794760),
(52, 1, 3, 'Long time...', 1371697603);

-- --------------------------------------------------------

--
-- Table structure for table `names`
--

DROP TABLE IF EXISTS `names`;
CREATE TABLE IF NOT EXISTS `names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `names`
--

INSERT INTO `names` (`id`, `name`) VALUES
(1, 'Atul Dubey'),
(2, 'Atul Colin'),
(3, 'Dale Garrett'),
(4, 'Billy Garrett');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post` text NOT NULL,
  `time` int(11) NOT NULL,
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `user_id`, `post`, `time`, `hidden`) VALUES
(3, 1, 'Hie', 1370604204, 0),
(4, 1, 'Finally, the wall is built...', 1370604452, 0),
(5, 1, 'But there''s a problem..... \r\n\r\nActually, there are many problems.\r\nThe posts should have an option of delete & hide.\r\nThe posts of the friends should also be visible on the wall...\r\n', 1370604536, 0),
(6, 3, 'It looks better now...\r\n', 1370607993, 0),
(7, 1, 'The posts should be displayed in the reverse order...', 1370610037, 0),
(8, 3, 'It''s going great.. I guess I like it...', 1370626051, 0),
(9, 1, 'Wassup??', 1370679942, 0),
(10, 8, 'Hie.. I''m here', 1370687876, 0),
(11, 9, 'Hie', 1370711515, 0),
(12, 1, 'Welcome to my wall....', 1370753341, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `surname`) VALUES
(1, 'atul', '32250170a0dca92d53ec9624f336ca24', 'Atul', 'Dubey'),
(2, 'sinister', '32250170a0dca92d53ec9624f336ca24', 'Sinister', 'Colin'),
(3, 'anjali', '367cde6ef1fc3e07ebe63476bb4a6c3f', 'Anjali', 'Bansiwal'),
(4, 'Sneha', 'e26bfda67f49ca1fc48f9b51003a5910', 'Sneha', 'Dubey'),
(5, 'roxy', 'e26bfda67f49ca1fc48f9b51003a5910', 'Roxy', 'Fellina'),
(6, 'sylvian', '367cde6ef1fc3e07ebe63476bb4a6c3f', 'Sylvian', 'George'),
(7, 'shiva', '61b4a64be663682e8cb037d9719ad8cd', 'Akash', 'Tiwari'),
(8, 'ashish', '7b69ad8a8999d4ca7c42b8a729fb0ffd', 'Ashish', 'Kumar 6'),
(9, 'gourab', '900150983cd24fb0d6963f7d28e17f72', 'Gourab', 'Patro');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
