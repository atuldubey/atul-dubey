<?php

require "core.inc.php";
require "connect.inc.php";

if(!loggedin()) {
	if(isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['password_again'])&&isset($_POST['firstname'])&&isset($_POST['surname'])){
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		$password_again = $_POST['password_again'];
		$firstname = $_POST['firstname'];
		$surname = $_POST['surname'];
		$password_hash = md5($password);

		if(!empty($username)&&!empty($password)&&!empty($password_again)&&!empty($firstname)&&!empty($surname)){
			if ($password!=$password_again) {
				echo "Passwords do not match... Retry!";
			}
			else{

				$query = "SELECT `username` FROM `users` WHERE `username`='$username'";
				$query_run = mysql_query($query);
				if (mysql_num_rows($query_run) ==1) {
					echo "The username ".'<i><b>'.$username.'</b></i>'.' already exists.';
				}
				else {
					$query = "INSERT INTO `users` VALUES ('',
						'".mysql_real_escape_string($username)."',
						'".mysql_real_escape_string($password_hash)."',
						'".mysql_real_escape_string($firstname)."',
						'".mysql_real_escape_string($surname)."')";

					if ($query_run = mysql_query($query)){
						header('Location: register_success.php');
					}
					else{
						echo "Sorry, we couldn't register you at the moment.<br>Please try again later...";
					}

				}

			}
		

		}
		else{
			echo "All the fields are required...";
		}
}


?>


<!DOCTYPE HTML>
<html>
<head>
	<title>Registration Page</title>
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

 
	<style type="text/css">

	.span12{
		background: purple;
		color: white;
		padding: 10px 0;
		text-align: center;
	}


	</style>


 </head>
 <body>


<div class="container">
<div class="row" id="main-content">

		<div class="span12"><h3>New User Registration</h3></div>
		
		<div class="span4" id="sidebar"><br><br>
			<div class="well">
			<form action="register.php" method="POST">
				<fieldset>
				<legend>Enter Details</legend>
				<input type='text' name='username' placeholder="Username..." class="span3" maxlength="30" value="<?php if(isset($username)){echo $username;} ?>">
				<br><br><input type='password' placeholder="Password..." class="span3" name='password' >
				<br><br><input type='password' placeholder="Re-enter Password..." class="span3" name='password_again'>
				<br><br><input type='text' placeholder="First Name" class="span3" name='firstname' maxlength="40" value="<?php if(isset($firstname)){echo $firstname;} ?>">
				<br><br><input type='text' placeholder="Surname" class="span3" name='surname' maxlength="40" value="<?php if(isset($surname)){echo $surname;} ?>">
				<br><br>
				<input type="submit" value="Register" class="btn btn-success">
				&nbsp;&nbsp;&nbsp;
				<input type="reset" value="Clear" class="btn btn-danger">
				</fieldset>
			</form>
			</div>
			<br><a href='index.php' class="btn btn-warning btn-large">Skip to Login Area</a>
		</div>		
</div>
</div>





<script src="http://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>



<?php
}
else if (loggedin()) {
	echo "You're already registered and logged in...";
}

?>