<!DOCTYPE HTML>
<html>
		  <head>
    		<title>Sinister :: Home</title>
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    		
    		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

 		  </head>
  			<body>

  				<div class = "container">

  					<header class="row">
  						<div class ="span12">

  							<nav class="navbar">
  								<div class="navbar-inner">
  									<a href="index.php" class="brand">Sinister's Website </a>
  									<ul class="nav">
  										<li class="divider-vertical"></li>
  										<li><a href="friends.php">Friends Area</a></li>
  										<li class="divider-vertical"></li>
  										<li><a href="chatroom.php">Chat Room</a></li>
  										<li class="divider-vertical"></li>
  										<li><a href="#info" role="button" data-toggle="modal">Add Info</a></li>
                      <li class="divider-vertical"></li>
                      <li><a href="logout.php">Log Out</a></li>
                      <li class="divider-vertical"></li>
                      
  									</ul>
  								</div>
  							</nav>
  						</div>
  					</header>
  				</div>
    				
    <div id="info" class="modal hide fade" aria-labelledby="modalLabel" aria-hidden="true">

      <div class='modal-header'>
        <h3 id="modalLabel">Add the Details
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="icon-remove"></i>
          </button>
          </h3>
      </div>

      <div class="modal-body">
          <form>

            <div class="controls controls-group">
              <input type="text" class="span3" placeholder="Nickname"><br><br>
              <input type="text" class="span3" placeholder="Birthplace"><br><br>
              Gender:
              <select class="span1">
                <option></option>
                <option>M</option>
                <option>F</option>
              </select>
              D.O.B.:
               <select class="span1" name="date">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
                <option>13</option>
                <option>14</option>
                <option>15</option>
                <option>16</option>
                <option>17</option>
                <option>18</option>
                <option>19</option>
                <option>20</option>
                <option>21</option>
                <option>22</option>
                <option>23</option>
                <option>24</option>
                <option>25</option>
                <option>26</option>
                <option>27</option>
                <option>28</option>
                <option>29</option>
                <option>30</option>
                <option>31</option>                                
              </select>
              /
              <select class="span1" name="month">
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>June</option>
                <option>July</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Dec</option>
              </select>
              /
              <input type="text" class="span1" placeholder="YYYY">
              

              <br><br>


              <input type="text" class="span3" placeholder="E-mail id"><br><br>
              <input type="text" class="span3" placeholder="Confirm e-mail id"><br>

            </div>

          </form> 

      </div>

      <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Submit</button>
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
      </div>



    </div>


    				<script src="http://code.jquery.com/jquery.js"></script>
    				<script src="js/bootstrap.min.js"></script>
  			</body>
		</html>

